variable "environment" {
  type        = string
  description = "Environment for the ASG"
}

variable "asg_name" {
  type        = string
  description = "Name to be used by ASG instances"
}

variable "asg_image_id" {
  type        = string
  description = "Image to be used by ASG instances"
}

variable "asg_instance_type" {
  type        = string
  description = "Instance type for the ASG instances"
}

variable "asg_instance_profile_name" {
  type        = string
  description = "Instance profile for ASG instances"
}

variable "asg_key_name" {
  type        = string
  description = "Keypair used for the ASG"
}

variable "asg_security_groups" {
  type        = list
  description = "List of SG to be used by ASG instances"
}

variable "asg_volume_size" {
  type        = number
  description = "ASG instances volume size"
  default     = 8
}

variable "asg_max_size" {
  type        = number
  description = "ASG max capacity"
}

variable "asg_min_size" {
  type        = number
  description = "ASG min capacity"
}

variable "asg_default_cooldown" {
  type        = number
  description = "ASG default cooldown"
  default     = 10
}

variable "asg_health_check_grace_period" {
  type        = number
  description = "Healthcheck grace period for the ASG"
  default     = 300
}

variable "asg_subnet_ids" {
  type        = list
  description = "Subnet ids where ASG will be deployed"
}

variable "asg_termination_policies" {
  type        = list
  description = "ASG applied termination policies"
  default     = ["Default"]
}